/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <fstream>
#include <sstream>

//==============================================================================

std::string readFileAsString(const char* filePath)
{
    std::ifstream inputStream;
    inputStream.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    
    try
    {
        inputStream.open(filePath);
        std::stringstream ss;
        ss << inputStream.rdbuf();
        inputStream.close();
        return ss.str();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << "ERROR::FILE_NOT_SUCCESFULLY_READ: " << filePath << std::endl;
    }
    
    return "";
}

/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public OpenGLAppComponent
{
public:
    //==============================================================================
    MainComponent()
    {
        // Make sure you set the size of the component after
        // you add any child components.
        setSize (800, 600);
        
        openGLContext.setOpenGLVersionRequired(juce::OpenGLContext::openGL3_2);
    }
    
    ~MainComponent()
    {
        // This shuts down the GL system and stops the rendering calls.
        shutdownOpenGL();
    }

    //==============================================================================
    void initialise() override
    {
        char* v = (char*) glGetString(GL_VERSION);
        DBG("OpenGL version: " << v);
        
        buildProgram();
        describeData();
        
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    void shutdown() override
    {
        deleteVertexArraysAndBuffers();
    }
    
    void render() override
    {
        OpenGLHelpers::clear (Colours::black);
        
        renderTriangle();
    }

    //==============================================================================
    void paint (Graphics& g) override {}
    void resized() override {}

private:
    //==============================================================================
    
    OpenGLShaderProgram shaderProgram { openGLContext };
    
    GLuint VAO;
    GLuint VBO;
    GLuint EBO;
    
    // Build program ----------------------------------------------------------------
    
    void buildProgram()
    {
        shaderProgram.addVertexShader(readFileAsString("../../../../Shaders/Tut3.3.vs"));
        shaderProgram.addFragmentShader(readFileAsString("../../../../Shaders/Tut3.3.fs"));
        shaderProgram.link();
    }

    // Describe data ----------------------------------------------------------------
    void describeData()
    {
        GLfloat vertices[] = {
            0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f, // Prawy górny
            0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // Prawy dolny
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f,// Lewy dolny
            -0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 1.0f  // Lewy górny
        };
        
        GLuint indices[] = { // Zauważ, że zaczynamy od 0!
            0, 1, 3, // Pierwszy trójkąt
            1, 2, 3 // Drugi trójkąt
        };
        
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &EBO);
        
        glBindVertexArray(VAO);
        {
            glBindBuffer(GL_ARRAY_BUFFER, VBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
                
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
                
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)( 3 * sizeof(float)));
            glEnableVertexAttribArray(1);
    
            // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            
            // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
            //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            
        }
        // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
        // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
        glBindVertexArray(0);
    }
    
    // render -----------------------------------------------------------------------
    
    void renderTriangle()
    {        
        shaderProgram.use();

        float m = std::sin(Time::currentTimeMillis() / 1000.) / 2.f;
        shaderProgram.setUniform("ourColor", 2.0f * m, m + 0.5, m + 1.0f, 1.0f);
        
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }
    
    // clean -------------------------------------------------------------------------
    
    void deleteVertexArraysAndBuffers()
    {
        glDeleteVertexArrays(1, &VAO);
        glDeleteBuffers(1, &VBO);
        glDeleteBuffers(1, &EBO);
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
