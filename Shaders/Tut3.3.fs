#version 330 core
in vec3 vColor;
out vec4 color;
uniform vec4 ourColor;

void main()
{
    color = ourColor * vec4(vColor, 1.0f);
}
