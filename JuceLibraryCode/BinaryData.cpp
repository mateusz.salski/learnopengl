/* ==================================== JUCER_BINARY_RESOURCE ====================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

namespace BinaryData
{

//================== Tut3.3.fs ==================
static const unsigned char temp_binary_data_0[] =
"";

const char* Tut3_3_fs = (const char*) temp_binary_data_0;

//================== Tut3.3.vs ==================
static const unsigned char temp_binary_data_1[] =
"";

const char* Tut3_3_vs = (const char*) temp_binary_data_1;


const char* getNamedResource (const char* resourceNameUTF8, int& numBytes)
{
    unsigned int hash = 0;

    if (resourceNameUTF8 != nullptr)
        while (*resourceNameUTF8 != 0)
            hash = 31 * hash + (unsigned int) *resourceNameUTF8++;

    switch (hash)
    {
        case 0x750b4898:  numBytes = 0; return Tut3_3_fs;
        case 0x750b4a88:  numBytes = 0; return Tut3_3_vs;
        default: break;
    }

    numBytes = 0;
    return nullptr;
}

const char* namedResourceList[] =
{
    "Tut3_3_fs",
    "Tut3_3_vs"
};

const char* originalFilenames[] =
{
    "Tut3.3.fs",
    "Tut3.3.vs"
};

const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8)
{
    for (unsigned int i = 0; i < (sizeof (namedResourceList) / sizeof (namedResourceList[0])); ++i)
    {
        if (namedResourceList[i] == resourceNameUTF8)
            return originalFilenames[i];
    }

    return nullptr;
}

}
